package com.example.gio.layouts;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    CheckBox check1, check2;
    RadioButton radio1, radio2, radio3;
    RadioGroup radioGroup;
    ImageButton imageButton;
    ToggleButton toggleButton;
    Switch aSwitch;
    ConstraintLayout fondo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        check1 = findViewById(R.id.check1);
        check2 = findViewById(R.id.check2);
        radioGroup = findViewById(R.id.groupRadio);
        imageButton = findViewById(R.id.btnVerificar);
        toggleButton = findViewById(R.id.toggleHabilitar);
        aSwitch = findViewById(R.id.swCambiarFondo);
        fondo = findViewById(R.id.fondo);
        radio1 = findViewById(R.id.radio1);
        radio2 = findViewById(R.id.radio2);
        radio3 = findViewById(R.id.radio3);
        aSwitch.setOnCheckedChangeListener(this);
        toggleButton.setOnCheckedChangeListener(this);
        imageButton.setClickable(false);
    }

    public void verificarValores(View view) {
        String mensaje = "";
        mensaje += check1.isChecked() ? " check1 checked ": "";
        mensaje += check2.isChecked() ? " check2 checked ": "";
        mensaje += radioGroup.getCheckedRadioButtonId() == R.id.radio1 ? " radio1 checked ": "";
        mensaje += radioGroup.getCheckedRadioButtonId() == R.id.radio2 ? " radio2 checked ": "";
        mensaje += radioGroup.getCheckedRadioButtonId() == R.id.radio3 ? " radio3 checked ": "";

        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    public void habilitarBoton(View view) {
        int d = radioGroup.getCheckedRadioButtonId();
        if((check1.isChecked() || check2.isChecked()) && radioGroup.getCheckedRadioButtonId() != -1)
            imageButton.setClickable(true);
        else
            imageButton.setClickable(false);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId())
        {
            case R.id.toggleHabilitar:
                check1.setEnabled(isChecked);
                check2.setEnabled(isChecked);
                radio1.setEnabled(isChecked);
                radio2.setEnabled(isChecked);
                radio3.setEnabled(isChecked);
                imageButton.setEnabled(isChecked);
                aSwitch.setEnabled(isChecked);
                break;
            case R.id.swCambiarFondo:
                fondo.setBackgroundResource(isChecked ? R.color.colorPrimary : R.color.white);
                break;

        }
    }
}
